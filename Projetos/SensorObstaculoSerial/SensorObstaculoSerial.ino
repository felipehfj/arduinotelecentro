#include <Ultrasonic.h>

#define QUANTIDADELEITURAS  1
#define DISTANCIAMINIMA     100
#define DISTANCIAMAXIMA     200

int valorFrente   = 0;
int valorDireita  = 0;
int valorEsquerda = 0;

Ultrasonic ultrasonicFrente(9, 8);
Ultrasonic ultrasonicDireita(7, 6);
Ultrasonic ultrasonicEsquerda(5, 4);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:

  valorFrente = obterValorFrente();
  if (valorFrente < DISTANCIAMINIMA) {
    valorDireita  = obterValorDireita();
    valorEsquerda = obterValorEsquerda();

    if ((valorDireita > DISTANCIAMAXIMA) && (valorEsquerda > DISTANCIAMAXIMA)){
      Serial.println(VireAdireitaOUvireAesquerda());
    }
    
    if ((valorDireita > DISTANCIAMAXIMA) && (valorEsquerda < DISTANCIAMINIMA)){
      Serial.println(VireAdireita());
    }
    
    if ((valorDireita < DISTANCIAMINIMA) && (valorEsquerda > DISTANCIAMAXIMA)){
      Serial.println(VireAesquerda());
    }
    
    if ((valorDireita < DISTANCIAMINIMA) && (valorEsquerda < DISTANCIAMINIMA)){
      Serial.println(Retorne());
    }
  }

  Serial.print("Distancia a Frente(cm): ");
  // Pass INC as a parameter to get the distance in inches
  Serial.println(valorFrente);

  Serial.print("Distancia a Direita(cm): ");
  // Pass INC as a parameter to get the distance in inches
  Serial.println(valorDireita);

  Serial.print("Distancia a Esquerda(cm): ");
  // Pass INC as a parameter to get the distance in inches
  Serial.println(valorEsquerda);

  delay(1000);

}

int obterValorFrente() {
  unsigned int valor;
  
  for (int i = 0; i < QUANTIDADELEITURAS; i++) valor += ultrasonicFrente.distanceRead();

  return (int)(valor / QUANTIDADELEITURAS);
}

int obterValorDireita() {
  unsigned int valor;
  
  for (int i = 0; i < QUANTIDADELEITURAS; i++) valor += ultrasonicDireita.distanceRead();

  return (int)(valor / QUANTIDADELEITURAS);
}

int obterValorEsquerda() {
  unsigned int valor;
  
  for (int i = 0; i < QUANTIDADELEITURAS; i++) valor += ultrasonicEsquerda.distanceRead();

  return (int)(valor / QUANTIDADELEITURAS);
}

int Retorne() {
  return 1;
}

int VireAdireita() {
  return 2;
}

int VireAesquerda() {
  return 3;
}

int VireAdireitaOUvireAesquerda() {
  return 4;
}
