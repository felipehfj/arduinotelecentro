#include <LED.h>
#include <SerialCommand.h>
#include <Servo.h>

const byte POSICAOINICIAL1 = 0;
const byte POSICAOFINAL1   = 90;
const byte POSICAOINICIAL2 = 0;
const byte POSICAOFINAL2   = 70;

const byte VELOCIDADEMOVIMENTOSERVO = 10;

SerialCommand comando;

byte ledPin1 = 2;
byte ledPin2 = 3;
byte ledPin3 = 4;
byte ledPin4 = 5;

LED led1(ledPin1);
LED led2(ledPin2);
LED led3(ledPin3);
LED led4(ledPin4);

byte servoPin1 = 12;
byte servoPin2 = 13;

Servo servo1;
Servo servo2;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  servo1.attach(servoPin1);
  servo2.attach(servoPin2);
  servo1.write(POSICAOINICIAL1);
  servo2.write(POSICAOINICIAL2);

  led1.off();
  led2.off();
  led3.off();
  led4.off();

  comando.addCommand("ACENDE", manipulaAcendimentoLeds);
  comando.addCommand("APAGA",  manipulaApagamentoLeds);
  comando.addCommand("ABRE",   manipulaAberturaPortoes);
  comando.addCommand("FECHA",  manipulaFechamentoPortoes);
  comando.addCommand("ESTADO", estado);
  comando.addDefaultHandler(comandoDesconhecido);
}

void loop() {
  // put your main code here, to run repeatedly:
  comando.readSerial();
}

void manipulaAcendimentoLeds() {
  char *argumento;
  argumento = comando.next();

  if (argumento != NULL) {
    switch (selecionaLed(argumento)) {

      case 1:
        led1.on();
        break;
      case 2:
        led2.on();
        break;
      case 3:
        led3.on();
        break;
      case 4:
        led4.on();
        break;
      case 5:
        led1.on();
        led2.on();
        led3.on();
        led4.on();
        break;
      case 0:
        Serial.println(F("Argumento invalido."));
        break;
    }

  } else {
    Serial.println(F("Nao foi fornecido argumento."));
  }

}

int selecionaLed(char * str) {
  if (strcmp("LED1", str) == 0) {
    return 1;
  } else if (strcmp("LED2", str) == 0) {
    return 2;
  } else if (strcmp("LED3", str) == 0) {
    return 3;
  } else if (strcmp("LED4", str) == 0) {
    return 4;
  } else if (strcmp("TODOS", str) == 0) {
    return 5;
  } else {
    return 0;
  }
}

void manipulaApagamentoLeds() {
  unsigned long intervalo;
  char *argumento;

  argumento = comando.next();

  if (argumento != NULL) {
    switch (selecionaLed(argumento)) {
      case 1:
        led1.off();
        break;
      case 2:
        led2.off();
        break;
      case 3:
        led3.off();
        break;
      case 4:
        led4.off();
        break;
      case 5:
        led1.off();
        led2.off();
        led3.off();
        led4.off();
        break;
      case 0:
        Serial.println(F("Argumento invalido."));
        break;
    }

  } else {
    Serial.println(F("Nao foi fornecido argumento."));
  }

}

void manipulaAberturaPortoes() {
  char *argumento;

  argumento = comando.next();

  if (argumento != NULL) {
    switch (selecionaServo(argumento)) {
      case 1:
        abrirPortao1();
        break;
      case 2:
        abrirPortao2();
        break;
      case 3:
        abrirPortao1();
        abrirPortao2();
        break;
      case 0:
        Serial.println(F("Argumento invalido."));
        break;
    }

  } else {
    Serial.println(F("Nao foi fornecido argumento."));
  }

}

void manipulaFechamentoPortoes() {
  char *argumento;
  argumento = comando.next();

  if (argumento != NULL) {
    switch (selecionaServo(argumento)) {
      case 1:
        fecharPortao1();
        break;
      case 2:
        fecharPortao2();
        break;
      case 3:
        fecharPortao1();
        fecharPortao2();
        break;
      case 0:
        Serial.println(F("Argumento invalido."));
        break;
    }

  } else {
    Serial.println(F("Nao foi fornecido argumento."));
  }

}

void comandoDesconhecido() {
  Serial.println(F("Comando desconhecido."));
}

int selecionaServo(char * str) {
  if (strcmp("SERVO1", str) == 0) {
    return 1;
  } else if (strcmp("SERVO2", str) == 0) {
    return 2;
  } else if (strcmp("TODOS", str) == 0) {
    return 3;
  } else {
    return 0;
  }
}


void abrirPortao1() {
  for (int i = POSICAOFINAL1; i >= POSICAOINICIAL1; i--) {
    servo1.write(i);
    delay(VELOCIDADEMOVIMENTOSERVO);
  }
}

void fecharPortao1() {
  for (int i = POSICAOINICIAL1; i <= POSICAOFINAL1; i++) {
    servo1.write(i);
    delay(VELOCIDADEMOVIMENTOSERVO);
  }
}

void abrirPortao2() {
  for (int i = POSICAOFINAL2; i >= POSICAOINICIAL2; i--) {
    servo2.write(i);
    delay(VELOCIDADEMOVIMENTOSERVO);
  }
}

void fecharPortao2() {
  for (int i = POSICAOINICIAL2; i <= POSICAOFINAL2; i++) {
    servo2.write(i);
    delay(VELOCIDADEMOVIMENTOSERVO);
  }
}

void estado() {
  Serial.print(F("LED1:"));
  Serial.print(led1.getState());
  Serial.print(F(", LED2:"));
  Serial.print(led2.getState());
  Serial.print(F(", LED3:"));
  Serial.print(led3.getState());
  Serial.print(F(", LED4:"));
  Serial.print(led4.getState());
  Serial.print(F(", SERVO1:"));
  Serial.print(servo1.read());
  Serial.print(F(", SERVO2:"));
  Serial.println(servo2.read());
}
