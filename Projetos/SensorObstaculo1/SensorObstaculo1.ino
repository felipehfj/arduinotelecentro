#include <PCM.h>
#include <Ultrasonic.h>
#include "vozes.h"

int menu = 0;
Ultrasonic ultrasonicFrente(9, 8);
Ultrasonic ultrasonicDireita(7, 6);
Ultrasonic ultrasonicEsquerda(5, 4);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:

  if (ultrasonicFrente.distanceRead() < 100) {
    
    if ((ultrasonicDireita.distanceRead() > 200) && (ultrasonicEsquerda.distanceRead() > 200)) {
      //Pare();
      VireAdireitaOUvireAesquerda();
    }
    if ((ultrasonicDireita.distanceRead() > 200) && (ultrasonicEsquerda.distanceRead() < 100)) {
      //Pare();
      VireAdireita();
    }
    if ((ultrasonicDireita.distanceRead() < 100) && (ultrasonicEsquerda.distanceRead() > 200)) {
      //Pare();
      VireAesquerda();
    }
    if ((ultrasonicDireita.distanceRead() < 100) && (ultrasonicEsquerda.distanceRead() <100)) {
      //Pare();
      Retorne();
    }

    while(ultrasonicFrente.distanceRead() < 100){      
      ;
    }
  }

  Serial.print("Distancia a Frente(cm): ");
  // Pass INC as a parameter to get the distance in inches
  Serial.println(ultrasonicFrente.distanceRead());

  Serial.print("Distancia a Direita(cm): ");
  // Pass INC as a parameter to get the distance in inches
  Serial.println(ultrasonicDireita.distanceRead());

  Serial.print("Distancia a Esquerda(cm): ");
  // Pass INC as a parameter to get the distance in inches
  Serial.println(ultrasonicEsquerda.distanceRead());
  
  delay(1000);

}

void Pare() {

  while (menu < 1) {
    menu++;

    Serial.println(menu);

    if (menu == 1) {
      startPlayback(pare, sizeof(pare));
    }

    for (int i = 0; i < 100; i++) {
      delay(10);
    }
  }
  menu = 0;

}

void Retorne() {

  while (menu < 1) {
    menu++;

    Serial.println(menu);

    if (menu == 1) {
      startPlayback(retorne, sizeof(retorne));
    }

    for (int i = 0; i < 100; i++) {
      delay(10);
    }
  }
  menu = 0;

}

void VireAdireita() {

  while (menu < 3) {
    menu++;

    Serial.println(menu);

    if (menu == 1) {
      startPlayback(vire, sizeof(vire));
    }

    if (menu == 2) {
      startPlayback(a, sizeof(a));
    }

    if (menu == 3) {
      startPlayback(direita, sizeof(direita));
    }

    for (int i = 0; i < 100; i++) {
      delay(10);
    }
  }
  menu = 0;

}

void VireAesquerda() {

  while (menu < 3) {
    menu++;

    Serial.println(menu);

    if (menu == 1) {
      startPlayback(vire, sizeof(vire));
    }

    if (menu == 2) {
      startPlayback(a, sizeof(a));
    }

    if (menu == 3) {
      startPlayback(esquerda, sizeof(esquerda));
    }

    for (int i = 0; i < 100; i++) {
      delay(10);
    }
  }
  menu = 0;

}

void VireAdireitaOUvireAesquerda() {

  while (menu < 7) {
    menu++;

    Serial.println(menu);

    if (menu == 1) {
      startPlayback(vire, sizeof(vire));
    }

    if (menu == 2) {
      startPlayback(a, sizeof(a));
    }

    if (menu == 3) {
      startPlayback(direita, sizeof(direita));
    }

    if (menu == 4) {
      startPlayback(ou, sizeof(ou));
    }

    if (menu == 5) {
      startPlayback(vire, sizeof(vire));
    }

    if (menu == 6) {
      startPlayback(a, sizeof(a));
    }

    if (menu == 7) {
      startPlayback(esquerda, sizeof(esquerda));
    }


    for (int i = 0; i < 100; i++) {
      delay(10);
    }
  }
  menu = 0;

}
