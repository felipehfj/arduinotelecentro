#include <PCM.h>
#include <Ultrasonic.h>
#include "vozes.h"

#define QUANTIDADELEITURAS  1
#define DISTANCIAMINIMA     100
#define DISTANCIAMAXIMA     200
#define ESPERA              1000

int valorFrente   = 0;
int valorDireita  = 0;
int valorEsquerda = 0;

Ultrasonic ultrasonicFrente(9, 8);
Ultrasonic ultrasonicDireita(7, 6);
Ultrasonic ultrasonicEsquerda(5, 4);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:

  valorFrente = obterValorFrente();
  if (valorFrente < DISTANCIAMINIMA) {
    valorDireita  = obterValorDireita();
    valorEsquerda = obterValorEsquerda();
    Pare();    
    delay(ESPERA);
    if ((valorDireita > DISTANCIAMAXIMA) && (valorEsquerda > DISTANCIAMAXIMA)) VireAdireitaOUvireAesquerda();
    
    if ((valorDireita > DISTANCIAMAXIMA) && (valorEsquerda < DISTANCIAMINIMA)) VireAdireita();
    
    if ((valorDireita < DISTANCIAMINIMA) && (valorEsquerda > DISTANCIAMAXIMA)) VireAesquerda();
    
    if ((valorDireita < DISTANCIAMINIMA) && (valorEsquerda < DISTANCIAMINIMA)) Retorne();
  }

  Serial.print("Distancia a Frente(cm): ");
  // Pass INC as a parameter to get the distance in inches
  Serial.println(valorFrente);

  Serial.print("Distancia a Direita(cm): ");
  // Pass INC as a parameter to get the distance in inches
  Serial.println(valorDireita);

  Serial.print("Distancia a Esquerda(cm): ");
  // Pass INC as a parameter to get the distance in inches
  Serial.println(valorEsquerda);

  delay(100);

}

int obterValorFrente() {
  unsigned int valor;
  
  for (int i = 0; i < QUANTIDADELEITURAS; i++) valor += ultrasonicFrente.distanceRead();

  return (int)(valor / QUANTIDADELEITURAS);
}

int obterValorDireita() {
  unsigned int valor;
  
  for (int i = 0; i < QUANTIDADELEITURAS; i++) valor += ultrasonicDireita.distanceRead();

  return (int)(valor / QUANTIDADELEITURAS);
}

int obterValorEsquerda() {
  unsigned int valor;
  
  for (int i = 0; i < QUANTIDADELEITURAS; i++) valor += ultrasonicEsquerda.distanceRead();

  return (int)(valor / QUANTIDADELEITURAS);
}

void Pare() {
  int menu = 0;

  while (menu < 1) {
    menu++;

    Serial.println(menu);

    if (menu == 1) startPlayback(pare, sizeof(pare));

    for (int i = 0; i < 100; i++) delay(10);
  }
}

void Retorne() {
  int menu = 0;
  
  while (menu < 1) {
    menu++;

    Serial.println(menu);

    if (menu == 1) startPlayback(retorne, sizeof(retorne));

    for (int i = 0; i < 100; i++) delay(10);
  }
}

void VireAdireita() {
  int menu = 0;
  
  while (menu < 3) {
    menu++;

    Serial.println(menu);

    if (menu == 1) startPlayback(vire, sizeof(vire));

    if (menu == 2) startPlayback(a, sizeof(a));

    if (menu == 3) startPlayback(direita, sizeof(direita));

    for (int i = 0; i < 100; i++) {
      delay(10);
    }
  }
}

void VireAesquerda() {
  int menu = 0;
  
  while (menu < 3) {
    menu++;

    Serial.println(menu);

    if (menu == 1) startPlayback(vire, sizeof(vire));

    if (menu == 2) startPlayback(a, sizeof(a));

    if (menu == 3) startPlayback(esquerda, sizeof(esquerda));

    for (int i = 0; i < 100; i++) delay(10);
  }
}

void VireAdireitaOUvireAesquerda() {
  int menu = 0;
  
  while (menu < 6) {
    
    menu++;

    Serial.println(menu);

    if (menu == 1) startPlayback(vire, sizeof(vire));

    if (menu == 2) startPlayback(a, sizeof(a));

    if (menu == 3) startPlayback(direita, sizeof(direita));

    if (menu == 4) startPlayback(ou, sizeof(ou));

    if (menu == 5) startPlayback(a, sizeof(a));

    if (menu == 6) startPlayback(esquerda, sizeof(esquerda));

    for (int i = 0; i < 100; i++) delay(10);
  }
}
