#include <Ultrasonic.h>

const int DISTANCIAMAXIMA = 200;
const int DISTANCIAMINIMA = 100;

Ultrasonic ultrasonicFrente(9, 8);
Ultrasonic ultrasonicDireita(7, 6);
Ultrasonic ultrasonicEsquerda(5, 4);

unsigned int distanciaDireita  = 0;
unsigned int distanciaEsquerda = 0;
unsigned int distanciaFrente   = 0;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  determinarDirecao();

//  do {
//    distanciaFrente = ultrasonicFrente.ping_cm();
//    delay(40);
//  } while (distanciaFrente < DISTANCIAMINIMA);

  //imprimeDistancias() ;
}

void obterDistancias() {
  /*
     Expicação: http://howtomechatronics.com/tutorials/arduino/ultrasonic-sensor-hc-sr04/

     Velocidade do som em metros por segundo: 343,4 m/s a 25ºC
     Velocidade do som em centrimentro por microssegundos: 0.034 cm/us
     Distância máxima lida pelo sensor: 500 cm
     Tempo necessário para percorrer a distância máxima: 500cm / 0,034 cm/us =  14705,88 us ou 14,7 ms
     Porém, para a correta medida, é necessário acrescentar a distancia de ida e volta do som, isto é, 1000 cm.
     Logo, o tempo máximo necessário para a leitura é de 29,4 us (14,7 * 2)
     Para efetuar leituras de diversos sensores, se faz necessário acrescentar um delay do tempo máximo necessário para efetuar as leituras.
  */
  distanciaFrente = ultrasonicFrente.distanceRead();
  delay(40); // Adicionado uma margem de 10 ms com a intenção de dar tempo ao Arduino tempo processar os comandos.
  distanciaDireita = ultrasonicDireita.distanceRead();
  delay(40);
  distanciaEsquerda = ultrasonicEsquerda.distanceRead();
  delay(40);
}

int processarDistancias() {
  obterDistancias();

  /*
     Verifica se a distancia obtida na frente é menor que
     a distância mínima necessária para efetuar a parada
     Caso afirmativo entra no teste
  */
  if (distanciaFrente < DISTANCIAMINIMA) {

    /*
       Verifica se a distancia obtida na direita e na esquerda é maior que
       a distância segura necessária para se movimentar para as duas direções
       Caso afirmativo retorna 1
    */
    if ((distanciaDireita > DISTANCIAMAXIMA) && (distanciaEsquerda > DISTANCIAMAXIMA)) {
      return 1;
    }

    /*
       Verifica se a distancia obtida na direita é maior que a distancia maxima de segurança
       e a distancia na esquerda é menor que a menor distancia de segurança
       Caso afirmativo retorna 2
    */
    if ((distanciaDireita > DISTANCIAMAXIMA) && (distanciaEsquerda < DISTANCIAMINIMA)) {
      return 2;
    }

    /*
       Verifica se a distancia obtida na direita é menor que a distancia minima de segurança
       e a distancia na esquerda é maior que a maior distancia de segurança
       Caso afirmativo retorna 3
    */
    if ((distanciaDireita < DISTANCIAMINIMA) && (distanciaEsquerda > DISTANCIAMAXIMA)) {
      return 3;
    }

    /*
       Verifica se a distancia obtida na direita e na esquerda é menor que
       a distância segura necessária para se movimentar para as duas direções
       Caso afirmativo retorna 4
    */
    if ((distanciaDireita < DISTANCIAMINIMA) && (distanciaEsquerda < DISTANCIAMINIMA)) {
      return 4;
    }
  } else {
    return 0;
  }
}

void determinarDirecao() {
  int selecao = processarDistancias();

  switch (selecao) {
    case 1:
      Serial.println(vireDireitaOuEsquerda());
      break;
    case 2:
      Serial.println(vireDireita());
      break;
    case 3:
      Serial.println(vireEsquerda());
      break;
    case 4:
      Serial.println(retorne());
      break;
  }
}

String retorne() {
  return "Retorne";
}

String vireDireita() {
  return "Vire a Direita";
}

String vireEsquerda() {
  return "Vire a Esquerda";
}

String vireDireitaOuEsquerda() {
  return "Vire a Direita ou Esquerda";
}

void imprimeDistancias() {
  Serial.print("Distancia a Frente(cm): ");
  Serial.println(distanciaFrente);

  Serial.print("Distancia a Direita(cm): ");
  Serial.println(distanciaDireita);

  Serial.print("Distancia a Esquerda(cm): ");
  Serial.println(distanciaEsquerda);
}
