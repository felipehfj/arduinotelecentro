/*
    This sketch sends data via HTTP GET requests to data.sparkfun.com service.

    You need to get streamId and privateKey at data.sparkfun.com and paste them
    below. Or just customize this script to talk to other HTTP servers.

*/
#include <ESP8266WiFi.h>
//Carrega a biblioteca do sensor ultrassonico
#include <Ultrasonic.h>

//Inicializa o sensor nos pinos definidos acima
Ultrasonic ultrasonic(D3, D4);

const char *ssid     = "NodeMCU";
const char *password = "p@ssw0rd";
const uint16_t port = 80;
const char *host = "192.168.4.1"; // ip or dns

void setup() {
  Serial.begin(9600);
  delay(100);

  // We start by connecting to a WiFi network

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}


void loop() {

  int value = ultrasonic.distanceRead();
  if (value > 17) {
    Serial.print("connecting to ");
    Serial.println(host);

    // Use WiFiClient class to create TCP connections
    WiFiClient client;

    if (!client.connect(host, port)) {
      Serial.println("connection failed");
      return;
    }

    // We now create a URI for the request
    Serial.print("Posting DATA: ");
    Serial.println("Liga");

    // This will send the request to the server
    client.print(String("POST Liga") + "\r\n" +
                 "Host: " + host + "\r\n");
    delay(1000);

    // Read all the lines of the reply from server and print them to Serial
    //Serial.println("Responde:");
    //while(client.available()){
    //String line = client.readStringUntil('\r');
    //Serial.print(line);
    //}

    Serial.println();
    Serial.println("closing connection");

  }
  if (value < 7) {
    Serial.print("connecting to ");
    Serial.println(host);

    // Use WiFiClient class to create TCP connections
    WiFiClient client;

    if (!client.connect(host, port)) {
      Serial.println("connection failed");
      return;
    }

    // We now create a URI for the request
    Serial.print("Posting DATA: ");
    Serial.println("Desliga");

    // This will send the request to the server
    client.print(String("POST Desliga") + "\r\n" +
                 "Host: " + host + "\r\n" +
                 "Connection: close\r\n\r\n");
    delay(1000);

    // Read all the lines of the reply from server and print them to Serial
    //Serial.println("Responde:");
    //while(client.available()){
    //String line = client.readStringUntil('\r');
    //Serial.print(line);
    //}

    Serial.println();
    Serial.println("closing connection");
  }
}
