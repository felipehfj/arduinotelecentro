//Programa : Agua_Inteligente
#include <ESP8266WiFi.h>
//Carrega a biblioteca do sensor ultrassonico
#include <Ultrasonic.h>

//Inicializa o sensor nos pinos definidos acima
Ultrasonic ultrasonic(D4, D3);

const char *ssid = "NodeMCU";
const char *password = "p@ssw0rd";
String ultra2 = "Medicao nao efetuada";

WiFiServer server(80);

//Porta ligada ao pino D1 do modulo
int porta_rele1 = D5;
//Porta ligada ao pino D2 do modulo
int porta_rele2 = D6;

void setup() {
  Serial.begin(9600);
  delay(100);

  //Define pinos para o rele como saida e mantem os reles desligados
  pinMode(porta_rele1, OUTPUT);
  digitalWrite(porta_rele1, HIGH); //Desliga rele 1
  pinMode(porta_rele2, OUTPUT);
  digitalWrite(porta_rele2, HIGH); //Desliga rele 2

  WiFi.softAP(ssid, password);

  // Wait for connection
  //  while (WiFi.status() != WL_CONNECTED) {
  //    delay(500);
  //    Serial.print(".");
  //  }
  Serial.println("");
  Serial.print("WiFi created: ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.softAPIP());

  // Start the server
  server.begin();
  Serial.println("Server started");
}

void loop() {
  // Check if a client has connected

  WiFiClient client = server.available();
  if (!client) {
    return;
  }

  // Wait until the client sends some data
  Serial.println("New client connected.");
  while (!client.available()) {
    delay(100);
  }

  //Ultrasonic variable
  String ultra1 = "Medicao nao efetuada";

  //Print Client IP
  String addy = client.remoteIP().toString();
  Serial.println("IP:" + addy);

  // Read the first line of the request
  String request = client.readStringUntil('\r');
  Serial.println(request);
  client.flush();

  // Match the request
  if (request.indexOf("/rele1On") != -1) {
    digitalWrite(porta_rele1, LOW); //Liga rele 1
  }
  if (request.indexOf("/rele1Off") != -1) {
    digitalWrite(porta_rele1, HIGH); //Desliga rele 1
  }
  if (request.indexOf("/rele2On") != -1) {
    digitalWrite(porta_rele2, LOW); //Liga rele 2
  }
  if (request.indexOf("/rele2Off") != -1) {
    digitalWrite(porta_rele2, HIGH); //Desliga rele 2
  }
  if (request.indexOf("POST") != -1) {
    ultra2 = request.substring(5);
    //Serial.print("Altura reservatorio 2: ");
    //Serial.println(ultra2);
  }

  // Set rele according to the request
  //digitalWrite(porta_rele, value);

  // Return the response
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println(""); // do not forget this one
  client.println("<!DOCTYPE HTML>");
  client.println("<html>");
  client.print("<h1>Controle de Bombas</h1> ");
  client.println("<p>Bomba #1 <a href=\"/rele1On\"><button>ON</button></a>&nbsp;<a href=\"/rele1Off\"><button>OFF</button></a></p>");
  client.println("<p>Bomba #2 <a href=\"/rele2On\"><button>ON</button></a>&nbsp;<a href=\"/rele2Off\"><button>OFF</button></a></p>");
  if (request.indexOf("/UltraSonic1") != -1) {
    ultra1 = String(ultrasonic.distanceRead());
    client.println("<p>(Reservatorio Principal)  Altura da agua: " + ultra1 + " cm <a href=\"/UltraSonic1\"><button>Medir</button></a></p>");
  }
  else {
    client.println("<p>(Reservatorio Principal)  Altura da agua: " + ultra1 + " <a href=\"/UltraSonic1\"><button>Medir</button></a></p>");
  }
  client.println("<p>(Reservatorio Secundario) Altura da agua: " + ultra2 + " cm </p>");
  client.println("</html>");

  if (ultra2.equals("Liga")) {
    digitalWrite(porta_rele1, LOW); //Liga rele 1
  }
  if ( ultra2.equals("Desliga")) {
    digitalWrite(porta_rele1, HIGH); //Desliga rele 1
  }

  delay(300);
  Serial.println("Client disconnected.");
  Serial.println("");
}
