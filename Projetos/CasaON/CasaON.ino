#include <LED.h>
#include <Servo.h>

const byte POSICAOINICIAL1 = 0;
const byte POSICAOFINAL1   = 90;
const byte POSICAOINICIAL2 = 0;
const byte POSICAOFINAL2   = 70;

const byte VELOCIDADEMOVIMENTOSERVO = 10;

byte ledPin1 = 2;
byte ledPin2 = 3;
byte ledPin3 = 4;
byte ledPin4 = 5;

LED led1(ledPin1);
LED led2(ledPin2);
LED led3(ledPin3);
LED led4(ledPin4);

byte servoPin1 = 12;
byte servoPin2 = 13;

Servo servo1;
Servo servo2;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  servo1.attach(servoPin1);
  servo2.attach(servoPin2);
  servo1.write(POSICAOINICIAL1);
  servo2.write(POSICAOINICIAL2);

  led1.off();
  led2.off();
  led3.off();
  led4.off();
}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available() > 0) {
    char sel = Serial.read();

    switch (sel) {
      case 'a':
        led1.on();
        break;
      case 'b':
        led1.off();
        break;
      case 'c':
        led2.on();
        break;
      case 'd':
        led2.off();
        break;
      case 'e':
        led3.on();
        break;
      case 'f':
        led3.off();
        break;
      case 'g':
        led4.on();
        break;
      case 'h':
        led4.off();
        break;
      case 'i':
        led1.on();
        led2.on();
        led3.on();
        led4.on();
        break;
      case 'j':
        led1.off();
        led2.off();
        led3.off();
        led4.off();
        break;
      case 'k':
        abrirPortao1();
        break;
      case 'l':
        fecharPortao1();
        break;
      case 'm':
        abrirPortao2();
        break;
      case 'n':
        fecharPortao2();
        break;
      case 'o':
        abrirPortao1();
        abrirPortao2();
        break;
      case 'p':
        fecharPortao1();
        fecharPortao2();
        break;
    }
  }
}


void abrirPortao1() {
  if (servo1.read() == POSICAOFINAL1) {
    for (int i = POSICAOFINAL1; i >= POSICAOINICIAL1; i--) {
      servo1.write(i);
      delay(VELOCIDADEMOVIMENTOSERVO);
    }
  }
}

void fecharPortao1() {
  if (servo1.read() == POSICAOINICIAL1) {
    for (int i = POSICAOINICIAL1; i <= POSICAOFINAL1; i++) {
      servo1.write(i);
      delay(VELOCIDADEMOVIMENTOSERVO);
    }
  }
}

void abrirPortao2() {
  if (servo2.read() == POSICAOFINAL2) {
    for (int i = POSICAOFINAL2; i >= POSICAOINICIAL2; i--) {
      servo2.write(i);
      delay(VELOCIDADEMOVIMENTOSERVO);
    }
  }
}

void fecharPortao2() {
  if (servo2.read() == POSICAOINICIAL2) {
    for (int i = POSICAOINICIAL2; i <= POSICAOFINAL2; i++) {
      servo2.write(i);
      delay(VELOCIDADEMOVIMENTOSERVO);
    }
  }
}
