#define QUANTIDADEPONTOS 5

bool debug = false;

typedef struct sensor {
  byte pino = 0;
  unsigned int limiar = 0;
  unsigned int dado = 0;
} SensorLDR;

SensorLDR sensores[QUANTIDADEPONTOS];

void inicializarSensores(SensorLDR * sensor) {
  for (int i = 0; i < QUANTIDADEPONTOS; i++) {
    sensor[i].pino = i + A0;
    pinMode(sensor[i].pino, INPUT);
    sensor[i].limiar = analogRead(sensor[i].pino);

  }
}

void lerDadosSensores(SensorLDR * sensor) {
  for (int i = 0; i < QUANTIDADEPONTOS; i++) {
    sensor[i].dado = analogRead(sensor[i].pino);
  }
}

void manipulaSensores(SensorLDR  * sensor) {
  lerDadosSensores(sensor);

  for (int i = 0; i < QUANTIDADEPONTOS; i++) {
    //sensor[i].dado = analogRead(sensor[i].pino);
    if (sensor[i].dado > sensor[i].limiar + 100) {
      char c = (char)'a'+ i;
      Serial.println(c);
    }
  }
}

void imprimeDadosSensoresCompleto(SensorLDR * sensor) {
  for (int i = 0; i < QUANTIDADEPONTOS; i++) {
    Serial.print("Sensor[");
    Serial.print(i + 1);
    Serial.print("] ");
    Serial.print("{ Pino: ");
    Serial.print(sensor[i].pino);
    Serial.print(", Limiar: ");
    Serial.print(sensor[i].limiar);
    Serial.print(", Dado: ");
    Serial.print(sensor[i].dado);
    Serial.println(" }");
  }
}

void pausa(int centesimos) {
  delay(centesimos * 100);
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  inicializarSensores(sensores);
  if (debug) {
    lerDadosSensores(sensores);
    imprimeDadosSensoresCompleto(sensores);
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  manipulaSensores(sensores);
  
  if(debug) imprimeDadosSensoresCompleto(sensores);
  
  pausa(1);
}
