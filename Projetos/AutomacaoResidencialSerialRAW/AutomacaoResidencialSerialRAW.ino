#include <LED.h>
#include <Servo.h>

const byte POSICAOINICIAL1 = 0;
const byte POSICAOFINAL1   = 90;
const byte POSICAOINICIAL2 = 0;
const byte POSICAOFINAL2   = 70;

const byte VELOCIDADEMOVIMENTOSERVO = 10;

byte ledPin1 = 2;
byte ledPin2 = 3;
byte ledPin3 = 4;
byte ledPin4 = 5;

LED led1(ledPin1);
LED led2(ledPin2);
LED led3(ledPin3);
LED led4(ledPin4);

byte servoPin1 = 12;
byte servoPin2 = 13;

Servo servo1;
Servo servo2;

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete

String acao = "";
String objeto = "";

void setup() {
  // initialize serial:
  Serial.begin(9600);
  // reserve 200 bytes for the inputString:
  inputString.reserve(200);

  servo1.attach(servoPin1);
  servo2.attach(servoPin2);
  servo1.write(POSICAOINICIAL1);
  servo2.write(POSICAOINICIAL2);

  led1.off();
  led2.off();
  led3.off();
  led4.off();
}

void loop() {
  // print the string when a newline arrives:
  if (stringComplete) {
    int sel = 0;

    Serial.println(inputString);

    acao   = inputString.substring(0, inputString.indexOf(' '));
    objeto = inputString.substring(inputString.indexOf(' ') + 1, inputString.indexOf('$'));

    Serial.println(acao);
    Serial.println(objeto);

    sel = manipulaAcao(acao);

    Serial.println(sel);

    switch (sel) {
      case 1: //ACENDE
        manipulaAcendimentoLeds();
        break;
      case 2: //APAGA
        manipulaApagamentoLeds();
        break;
      case 3: //ABRE
        manipulaAberturaPortoes();
        break;
      case 4: //FECHA
        manipulaFechamentoPortoes();
        break;
    }

    // clear the string:
    acao = "";
    objeto = "";
    inputString = "";
    stringComplete = false;
  }
}

/*
  SerialEvent occurs whenever a new data comes in the
  hardware serial RX.  This routine is run between each
  time loop() runs, so using delay inside loop can delay
  response.  Multiple bytes of data may be available.
*/
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '$') {
      stringComplete = true;
    }
  }
}

void manipulaAcendimentoLeds() {
  Serial.println(selecionaLed(objeto));
  
  switch (selecionaLed(objeto)) {
    case 1:
      led1.on();
      break;
    case 2:
      led2.on();
      break;
    case 3:
      led3.on();
      break;
    case 4:
      led4.on();
      break;
    case 5:
      led1.on();
      led2.on();
      led3.on();
      led4.on();
      break;
    case 0:
      Serial.println(F("Argumento invalido."));
      break;
  }
}

int selecionaLed(String str) {
  if (str.equals("LED1")) {
    return 1;
  } else if (str.equals("LED2")) {
    return 2;
  } else if (str.equals("LED3")) {
    return 3;
  } else if (str.equals("LED4")) {
    return 4;
  } else if (str.equals("TODOS")) {
    return 5;
  } else {
    return 0;
  }
}

int manipulaAcao(String str) {
  if (str.equals("ACENDE")) {
    return 1;
  } else if (str.equals("APAGA")) {
    return 2;
  } else if (str.equals("ABRE")) {
    return 3;
  } else if (str.equals("FECHA")) {
    return 4;
  } else {
    return 0;
  }
}

void manipulaApagamentoLeds() {
  switch (selecionaLed(objeto)) {
    case 1:
      led1.off();
      break;
    case 2:
      led2.off();
      break;
    case 3:
      led3.off();
      break;
    case 4:
      led4.off();
      break;
    case 5:
      led1.off();
      led2.off();
      led3.off();
      led4.off();
      break;
    case 0:
      Serial.println(F("Argumento invalido."));
      break;
  }
}

void manipulaAberturaPortoes() {
  switch (selecionaServo(objeto)) {
    case 1:
      abrirPortao1();
      break;
    case 2:
      abrirPortao2();
      break;
    case 3:
      abrirPortao1();
      abrirPortao2();
      break;
    case 0:
      Serial.println(F("Argumento invalido."));
      break;
  }
}

void manipulaFechamentoPortoes() {
  switch (selecionaServo(objeto)) {
    case 1:
      fecharPortao1();
      break;
    case 2:
      fecharPortao2();
      break;
    case 3:
      fecharPortao1();
      fecharPortao2();
      break;
    case 0:
      Serial.println(F("Argumento invalido."));
      break;
  }
}

void comandoDesconhecido() {
  Serial.println(F("Comando desconhecido."));
}

int selecionaServo(String str) {
  if (str.equals("SERVO1")) {
    return 1;
  } else if (str.equals("SERVO2")) {
    return 2;
  } else if (str.equals("TODOS")) {
    return 3;
  } else {
    return 0;
  }
}


void abrirPortao1() {
  for (int i = POSICAOFINAL1; i >= POSICAOINICIAL1; i--) {
    servo1.write(i);
    delay(VELOCIDADEMOVIMENTOSERVO);
  }
}

void fecharPortao1() {
  for (int i = POSICAOINICIAL1; i <= POSICAOFINAL1; i++) {
    servo1.write(i);
    delay(VELOCIDADEMOVIMENTOSERVO);
  }
}

void abrirPortao2() {
  for (int i = POSICAOFINAL2; i >= POSICAOINICIAL2; i--) {
    servo2.write(i);
    delay(VELOCIDADEMOVIMENTOSERVO);
  }
}

void fecharPortao2() {
  for (int i = POSICAOINICIAL2; i <= POSICAOFINAL2; i++) {
    servo2.write(i);
    delay(VELOCIDADEMOVIMENTOSERVO);
  }
}
