/*
  Projeto de automação residencial
  Autores: Antônio Junior, Felipe Ferreira & Milton Soares

**/

#include <LED.h>
#include <Bounce.h>
#include <Servo.h>

const int POSICAOINICIAL = 0;
const int POSICAOFINAL   = 90;

const long  ESPERA = 1 * 1000;
const long  ESPERACLIQUE = 5;

byte ledPin1 = 2;
byte ledPin2 = 3;
byte ledPin3 = 4;
byte ledPin4 = 5;

LED led1(ledPin1);
LED led2(ledPin2);
LED led3(ledPin3);
LED led4(ledPin4);

byte botaoPinLed1   = 6;
byte botaoPinLed2   = 7;
byte botaoPinLed3   = 8;
byte botaoPinLed4   = 9;
byte botaoPinServo1 = 10;
byte botaoPinServo2 = 11;

Bounce botaoLed1 = Bounce( botaoPinLed1, ESPERACLIQUE);
Bounce botaoLed2 = Bounce( botaoPinLed2, ESPERACLIQUE);
Bounce botaoLed3 = Bounce( botaoPinLed3, ESPERACLIQUE);
Bounce botaoLed4 = Bounce( botaoPinLed4, ESPERACLIQUE);

Bounce botaoServo1 = Bounce( botaoPinServo1, ESPERACLIQUE);
Bounce botaoServo2 = Bounce( botaoPinServo2, ESPERACLIQUE);

byte servoPin1 = 12;
byte servoPin2 = 13;

bool estadoServo1 = false;
bool estadoServo2 = false;

Servo servo1;
Servo servo2;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.print("Iniciando sistema...");

  pinMode(botaoPinLed1, INPUT);
  pinMode(botaoPinLed2, INPUT);
  pinMode(botaoPinLed3, INPUT);
  pinMode(botaoPinLed4, INPUT);
  pinMode(botaoPinServo1, INPUT);
  pinMode(botaoPinServo2, INPUT);

  servo1.attach(servoPin1);
  servo2.attach(servoPin2);

  Serial.print("iniciando leds...");
  testarLeds();
  delay(ESPERA);

  Serial.print("iniciando servos...");
  testarServos();
  delay(ESPERA);
  Serial.println("feito!");

}

void loop() {
  // put your main code here, to run repeatedly:

  atualizaTempoBotoes();

  if (botaoLed1.risingEdge()) {
    mudarLed1();
  }

  if (botaoLed2.risingEdge()) {
    mudarLed2();
  }

  if (botaoLed3.risingEdge()) {
    mudarLed3();
  }

  if (botaoLed4.risingEdge()) {
    mudarLed4();
  }

  if (botaoServo1.risingEdge()) {

    estadoServo1 = !estadoServo1;

    if (estadoServo1) {
      abrirPortao1();
    } else {
      fecharPortao1();
    }
  }

  if (botaoServo2.risingEdge()) {

    estadoServo2 = !estadoServo2;

    if (estadoServo2) {
      abrirPortao2();
    } else {
      fecharPortao2();
    }
  }
  
}

void testarLeds() {
  led1.on();
  delay(250);
  led1.off();

  led2.on();
  delay(250);
  led2.off();

  led3.on();
  delay(250);
  led3.off();

  led4.on();
  delay(250);
  led4.off();
}

void testarServos() {
  for (int i = POSICAOINICIAL; i <= POSICAOFINAL; i++) {
    servo1.write(i);
    delay(10);
  }

  for (int i = POSICAOFINAL; i >= POSICAOINICIAL; i--) {
    servo1.write(i);
    delay(10);
  }

  delay(ESPERA);

  for (int i = POSICAOINICIAL; i <= POSICAOFINAL; i++) {
    servo2.write(i);
    delay(10);
  }

  for (int i = POSICAOFINAL; i >= POSICAOINICIAL; i--) {
    servo2.write(i);
    delay(10);
  }

}

void atualizaTempoBotoes() {
  botaoLed1.update();
  botaoLed2.update();
  botaoLed3.update();
  botaoLed4.update();

  botaoServo1.update();
  botaoServo2.update();
}

void abrirPortao1() {
  Serial.print("Abrindo Portao 1...");
  for (int i = POSICAOINICIAL; i <= POSICAOFINAL; i++) {
    servo1.write(i);
    delay(10);
  }
  Serial.println("feito!");
}

void fecharPortao1() {
  Serial.print("Fechando Portao 1...");
  for (int i = POSICAOFINAL; i >= POSICAOINICIAL; i--) {
    servo1.write(i);
    delay(10);
  }
  Serial.println("feito!");
}

void abrirPortao2() {
  Serial.print("Abrindo Portao 2...");
  for (int i = POSICAOINICIAL; i <= POSICAOFINAL; i++) {
    servo2.write(i);
    delay(10);
  }
  Serial.println("feito!");
}

void fecharPortao2() {
  Serial.print("Fechando Portao 2...");
  for (int i = POSICAOFINAL; i >= POSICAOINICIAL; i--) {
    servo2.write(i);
    delay(10);
  }
  Serial.println("feito!");
}

void mudarLed1() {
  led1.toggle();

  if (led1.getState()) {
    Serial.println("Led 1 aceso!");
  } else {
    Serial.println("Led 1 apagado!");
  }
}

void mudarLed2() {
  led2.toggle();

  if (led2.getState()) {
    Serial.println("Led 2 aceso!");
  } else {
    Serial.println("Led 2 apagado!");
  }
}

void mudarLed3() {
  led3.toggle();

  if (led3.getState()) {
    Serial.println("Led 3 aceso!");
  } else {
    Serial.println("Led 3 apagado!");
  }
}

void mudarLed4() {
  led4.toggle();

  if (led4.getState()) {
    Serial.println("Led 4 aceso!");
  } else {
    Serial.println("Led 4 apagado!");
  }
}
