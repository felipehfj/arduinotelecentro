#include <Servo.h>

/*
 * Term�metro com Servo e LM35
 * Autor: Felipe Ferreira
 * Data: 20JUL2017
 * 
 */

Servo servo;

byte pinoServo    = 6;
byte pinoSensor   = A0;

int angulo        = 0;
int valorSensor   = 0;
float temperatura = 0;


void setup() {  
  // Define o pino do servo como saida
  pinMode(pinoServo, OUTPUT);
  
  // Define o pino do sensor como entrada
  pinMode(pinoSensor, INPUT);
  
  // Associa oservoo ao pinoServo
  servo.attach(pinoServo);
}

void loop() {
  // L� o valor anal�gico no pinoSensor (A0)
  valorSensor = analogRead(pinoSensor); 

  // Multiplica o valor lido pelo tens�o associada a cada milivolts. Temperatura saida em graus Celsius.
  // 500 / 1024  = 0,4882813
  temperatura = valorSensor * 0.4882813;

  // Transforma a temperatura lida para angulos entre 0 e 180�. 4 graus no servo por volt
  angulo = map(temperatura, 0, 45, 0, 180);

  // Movimenta o servo para o �ngulo correspondente na escala
  servo.write(angulo);

  // Aguarda 1 segundo para reiniciar
  delay(1000);
}