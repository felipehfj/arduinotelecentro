#include <LED.h>
#include <limits.h>
#include <Metro.h>
#include <SerialCommand.h>
#include <Servo.h>

#define POSICAOINICIAL1 0
#define POSICAOFINAL1   90
#define POSICAOINICIAL2 0
#define POSICAOFINAL2   70

#define ESPERA 1000

bool DEBUG = true;

SerialCommand comando;

byte ledPin1 = 2;
byte ledPin2 = 3;
byte ledPin3 = 4;
byte ledPin4 = 5;

LED led1(ledPin1);
LED led2(ledPin2);
LED led3(ledPin3);
LED led4(ledPin4);

bool isPiscaLed1 = false;
bool isPiscaLed2 = false;
bool isPiscaLed3 = false;
bool isPiscaLed4 = false;

Metro led1Metro = Metro(ULONG_MAX);
Metro led2Metro = Metro(ULONG_MAX);
Metro led3Metro = Metro(ULONG_MAX);
Metro led4Metro = Metro(ULONG_MAX);

byte servoPin1 = 12;
byte servoPin2 = 13;

bool estadoServo1 = false;
bool estadoServo2 = false;

Servo servo1;
Servo servo2;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.print("Iniciando sistema...");

  servo1.attach(servoPin1);
  servo2.attach(servoPin2);

  efetuaTestes();

  defineComandos();

  Serial.println("feito!");
}

void loop() {
  // put your main code here, to run repeatedly:
  comando.readSerial();
  manupulaIntervaloLeds();
}

void defineComandos() {
  if (DEBUG) Serial.print("definindo comandos...");
  comando.addCommand("ACENDE", manipulaAcendimentoLeds);
  comando.addCommand("APAGA",  manipulaApagamentoLeds);
  comando.addCommand("PISCA",  manipulaPiscaLeds);
  comando.addCommand("ABRE",   manipulaAberturaPortoes);
  comando.addCommand("FECHA",  manipulaFechamentoPortoes);
  comando.addCommand("ESTADO", estado);
  comando.addCommand("AJUDA",  ajuda);
  comando.addDefaultHandler(comandoDesconhecido);
}

void manupulaIntervaloLeds() {
  if (led1Metro.check() == 1 ) {
    if (isPiscaLed1) {
      if (led1.getState()) {
        led1.off();
      } else {
        led1.on();
      }
    } else {
      led1.off();
      led1Metro.interval(ULONG_MAX);
      led1Metro.reset();
    }
  }

  if (led2Metro.check() == 1 ) {
    if (isPiscaLed2) {
      if (led2.getState()) {
        led2.off();
      } else {
        led2.on();
      }
    } else {
      led2.off();
      led2Metro.interval(ULONG_MAX);
      led2Metro.reset();
    }
  }

  if (led3Metro.check() == 1 ) {
    if (isPiscaLed3) {
      if (led3.getState()) {
        led3.off();
      } else {
        led3.on();
      }
    } else {
      led3.off();
      led3Metro.interval(ULONG_MAX);
      led3Metro.reset();
    }
  }

  if (led4Metro.check() == 1 ) {
    if (isPiscaLed4) {
      if (led4.getState()) {
        led4.off();
      } else {
        led4.on();
      }
    } else {
      led4.off();
      led4Metro.interval(ULONG_MAX);
      led4Metro.reset();
    }
  }
}

void manipulaPiscaLeds() {
  unsigned long intervalo;
  char *argumento;

  argumento = comando.next();

  if (argumento != NULL) {
    switch (selecionaLed(argumento)) {
      case 1:
        argumento = comando.next();
        intervalo = constrain(atol(argumento), 1, ULONG_MAX);
        led1Metro.interval(intervalo * 1000);
        led1Metro.reset();
        isPiscaLed1 = true;
        Serial.print(F("Piscando LED1 por "));
        Serial.print(intervalo);
        Serial.println(F(" segundo(s)."));
        led1.on();
        break;
      case 2:
        argumento = comando.next();
        intervalo = constrain(atol(argumento), 1, ULONG_MAX);
        led2Metro.interval(intervalo * 1000);
        led2Metro.reset();
        isPiscaLed2 = true;
        Serial.print(F("Piscando LED2 por "));
        Serial.print(intervalo);
        Serial.println(F(" segundo(s)."));
        led2.on();
        break;
      case 3:
        argumento = comando.next();
        intervalo = constrain(atol(argumento), 1, ULONG_MAX);
        led3Metro.interval(intervalo * 1000);
        led3Metro.reset();
        isPiscaLed3 = true;
        Serial.print(F("Piscando LED3 por "));
        Serial.print(intervalo);
        Serial.println(F(" segundo(s)."));
        led3.on();
        break;
      case 4:
        argumento = comando.next();
        intervalo = constrain(atol(argumento), 1, ULONG_MAX);
        led4Metro.interval(intervalo * 1000);
        led4Metro.reset();
        isPiscaLed4 = true;
        Serial.print(F("Piscando LED4 por "));
        Serial.print(intervalo);
        Serial.println(F(" segundo(s)."));
        led4.on();
        break;
      case 5:
        argumento = comando.next();
        intervalo = constrain(atol(argumento), 1, ULONG_MAX);

        led1Metro.interval(intervalo * 1000);
        led2Metro.interval(intervalo * 1000);
        led3Metro.interval(intervalo * 1000);
        led4Metro.interval(intervalo * 1000);

        led1Metro.reset();
        led2Metro.reset();
        led3Metro.reset();
        led4Metro.reset();

        isPiscaLed1 = true;
        isPiscaLed2 = true;
        isPiscaLed3 = true;
        isPiscaLed4 = true;

        Serial.print(F("Piscando todos os LEDs por "));
        Serial.print(intervalo);
        Serial.println(F(" segundo(s)."));
        led1.on();
        led2.on();
        led3.on();
        led4.on();
        break;
      case 0:
        Serial.println(F("Argumento invalido."));
        break;
    }

  } else {
    Serial.println(F("Nao foi fornecido argumento."));
  }

}

void manipulaAcendimentoLeds() {
  unsigned long intervalo;
  char *argumento;

  argumento = comando.next();

  if (argumento != NULL) {
    switch (selecionaLed(argumento)) {

      case 1:
        argumento = comando.next();
        intervalo = constrain(atol(argumento), 1, ULONG_MAX);
        led1Metro.interval(intervalo * 1000);
        led1Metro.reset();
        isPiscaLed1 = false;
        Serial.print(F("Acendendo LED1 por "));
        Serial.print(intervalo);
        Serial.println(F(" segundo(s)."));
        led1.on();
        break;
      case 2:
        argumento = comando.next();
        intervalo = constrain(atol(argumento), 1, ULONG_MAX);
        led2Metro.interval(intervalo * 1000);
        led2Metro.reset();
        isPiscaLed2 = false;
        Serial.print(F("Acendendo LED2 por "));
        Serial.print(intervalo);
        Serial.println(F(" segundo(s)."));
        led2.on();
        break;
      case 3:
        argumento = comando.next();
        intervalo = constrain(atol(argumento), 1, ULONG_MAX);
        led3Metro.interval(intervalo * 1000);
        led3Metro.reset();
        isPiscaLed3 = false;
        Serial.print(F("Acendendo LED3 por "));
        Serial.print(intervalo);
        Serial.println(F(" segundo(s)."));
        led3.on();
        break;
      case 4:
        argumento = comando.next();
        intervalo = constrain(atol(argumento), 1, ULONG_MAX);
        led4Metro.interval(intervalo * 1000);
        led4Metro.reset();
        isPiscaLed4 = false;
        Serial.print(F("Acendendo LED4 por "));
        Serial.print(intervalo);
        Serial.println(F(" segundo(s)."));
        led4.on();
        break;
      case 5:
        argumento = comando.next();
        intervalo = constrain(atol(argumento), 1, ULONG_MAX);

        led1Metro.interval(intervalo * 1000);
        led2Metro.interval(intervalo * 1000);
        led3Metro.interval(intervalo * 1000);
        led4Metro.interval(intervalo * 1000);

        led1Metro.reset();
        led2Metro.reset();
        led3Metro.reset();
        led4Metro.reset();

        isPiscaLed1 = false;
        isPiscaLed2 = false;
        isPiscaLed3 = false;
        isPiscaLed4 = false;

        Serial.print(F("Acendendo todos os LEDs por "));
        Serial.print(intervalo);
        Serial.println(F(" segundo(s)."));
        led1.on();
        led2.on();
        led3.on();
        led4.on();
        break;
      case 0:
        Serial.println(F("Argumento invalido."));
        break;
    }

  } else {
    Serial.println(F("Nao foi fornecido argumento."));
  }

}

void manipulaApagamentoLeds() {
  unsigned long intervalo;
  char *argumento;

  argumento = comando.next();

  if (argumento != NULL) {
    switch (selecionaLed(argumento)) {
      case 1:
        led1Metro.interval(ULONG_MAX);
        led1Metro.reset();
        led1.off();
        Serial.println(F("Apagando LED1."));
        break;
      case 2:
        led2Metro.interval(ULONG_MAX);
        led2Metro.reset();
        led2.off();
        Serial.println(F("Apagando LED2."));
        break;
      case 3:
        led3Metro.interval(ULONG_MAX);
        led3Metro.reset();
        led3.off();
        Serial.println(F("Apagando LED3."));
        break;
      case 4:
        led4Metro.interval(ULONG_MAX);
        led4Metro.reset();
        led4.off();
        Serial.println(F("Apagando LED4."));
        break;
      case 5:
        led1Metro.interval(ULONG_MAX);
        led2Metro.interval(ULONG_MAX);
        led3Metro.interval(ULONG_MAX);
        led4Metro.interval(ULONG_MAX);

        led1Metro.reset();
        led2Metro.reset();
        led3Metro.reset();
        led4Metro.reset();

        led1.off();
        led2.off();
        led3.off();
        led4.off();
        Serial.println(F("Apagando todos os LEDs."));
        break;
      case 0:
        Serial.println(F("Argumento invalido."));
        break;
    }

  } else {
    Serial.println(F("Nao foi fornecido argumento."));
  }

}

void manipulaAberturaPortoes() {
  char *argumento;

  argumento = comando.next();

  if (argumento != NULL) {
    switch (selecionaServo(argumento)) {
      case 1:
        abrirPortao1();
        break;
      case 2:
        abrirPortao2();
        break;
      case 3:
        abrirPortao1();
        abrirPortao2();
        break;
      case 0:
        Serial.println(F("Argumento invalido."));
        break;
    }

  } else {
    Serial.println(F("Nao foi fornecido argumento."));
  }

}

void manipulaFechamentoPortoes() {
  char *argumento;

  argumento = comando.next();

  if (argumento != NULL) {
    switch (selecionaServo(argumento)) {
      case 1:
        fecharPortao1();
        break;
      case 2:
        fecharPortao2();
        break;
      case 3:
        fecharPortao1();
        fecharPortao2();
        break;
      case 0:
        Serial.println(F("Argumento invalido."));
        break;
    }

  } else {
    Serial.println(F("Nao foi fornecido argumento."));
  }

}

void comandoDesconhecido() {
  Serial.println(F("Comando desconhecido. Digite AJUDA para conhecer todos os comandos disponiveisa."));
}

int selecionaLed(char * str) {
  if (strcmp("LED1", str) == 0) {
    return 1;
  } else if (strcmp("LED2", str) == 0) {
    return 2;
  } else if (strcmp("LED3", str) == 0) {
    return 3;
  } else if (strcmp("LED4", str) == 0) {
    return 4;
  } else if (strcmp("TODOS", str) == 0) {
    return 5;
  } else {
    return 0;
  }
}

int selecionaServo(char * str) {
  if (strcmp("SERVO1", str) == 0) {
    return 1;
  } else if (strcmp("SERVO2", str) == 0) {
    return 2;
  } else if (strcmp("TODOS", str) == 0) {
    return 3;
  } else {
    return 0;
  }
}

void abrirPortao1() {
  Serial.print(F("Abrindo Portao 1..."));
  for (int i = POSICAOFINAL1; i >= POSICAOINICIAL1; i--) {
    servo1.write(i);
    delay(10);
  }
  Serial.println("feito!");
}

void fecharPortao1() {
  Serial.print(F("Fechando Portao 1..."));
  for (int i = POSICAOINICIAL1; i <= POSICAOFINAL1; i++) {
    servo1.write(i);
    delay(10);
  }
  Serial.println(F("feito!"));
}

void abrirPortao2() {
  Serial.print(F("Abrindo Portao 2..."));
  for (int i = POSICAOFINAL2; i >= POSICAOINICIAL2; i--) {
    servo2.write(i);
    delay(10);
  }
  Serial.println(F("feito!"));
}

void fecharPortao2() {
  Serial.print(F("Fechando Portao 2..."));
  for (int i = POSICAOINICIAL2; i <= POSICAOFINAL2; i++) {
    servo2.write(i);
    delay(10);
  }
  Serial.println(("feito!"));
}

void estado() {
  Serial.print(F("LED1:"));
  Serial.print(led1.getState());
  Serial.print(F(", LED2:"));
  Serial.print(led2.getState());
  Serial.print(F(", LED3:"));
  Serial.print(led3.getState());
  Serial.print(F(", LED4:"));
  Serial.print(led4.getState());
  Serial.print(F(", SERVO1:"));
  Serial.print(servo1.read());
  Serial.print(F(", SERVO2:"));
  Serial.println(servo2.read());
}

void testarLeds() {
  led1.on();
  delay(250);
  led1.off();

  led2.on();
  delay(250);
  led2.off();

  led3.on();
  delay(250);
  led3.off();

  led4.on();
  delay(250);
  led4.off();
}

void testarServos() {
  for (int i = POSICAOINICIAL1; i <= POSICAOFINAL1; i++) {
    servo1.write(i);
    delay(10);
  }

  for (int i = POSICAOFINAL1; i >= POSICAOINICIAL1; i--) {
    servo1.write(i);
    delay(10);
  }

  delay(ESPERA);

  for (int i = POSICAOINICIAL2; i <= POSICAOFINAL2; i++) {
    servo2.write(i);
    delay(10);
  }

  for (int i = POSICAOFINAL2; i >= POSICAOINICIAL2; i--) {
    servo2.write(i);
    delay(10);
  }

}

void efetuaTestes() {
  if (DEBUG) Serial.print(F("testando LEDs..."));
  testarLeds();
  delay(ESPERA);

  if (DEBUG) Serial.print(F("testando Servos..."));
  testarServos();
  delay(ESPERA);
}

void ajuda() {
  Serial.println(F("Os comandos disponiveis sao:\n\tACENDE LED[1-4] | TODOS [segundos] : Acende o LED selecionado por n segundos\n\tAPAGA LED[1-4] | TODOS : Apaga o LED selecionado\n\tPISCA LED[1-4] | TODOS [segundos] : Pisca o LED selecionado por n segundos\n\tABRE SERVO[1-2] | TODOS : Abre o portao selecionado\n\tFECHA SERVO[1-2] | TODOS : Fecha o portao selecionado\n\tAJUDA: exibe esta ajuda"));
}


