/*
 * Este projeto se destina a oferecer aos pedestres informações sonoras e visuais sobre o estado do semáforo de trânsito.
 * Tanto pedestres com necessidades especiais quanto os que não as possuem se servirão das informações oferecidas pelo semáforo.
 * Funcionamento:
 * Quando um pedestre deseja atravessar a via, ele pressiona o botão disponível no suporte do semáforo.
 * Ao receber tal informação, o semáforo retorna ao usuário, por meio de voz eletrônica, a solicitação de interveção efetuada e o tempo necessário para o atendimento.
 * No fechamento do sinal para os carros, o semáforo informa ao usuário que a sinalização para a interrupção do trafego dos carros foi efetuada e que ele esta liberado para atravessar a via.
 * Quando restam 10 segundos para o fechamento do sinal para os pedestres, o semáforo emite bips sonoros a fim de alertar ao usuário do fechamento.
 * No fechamento do semáforo para os transeuntes, o sistema emite essa informação por meio de voz eletrônica.
 * 
 */


#include "pitches.h"
/*========== Variáveis globais ==========*/

// Definições de tempo (em segundos)
unsigned int  tempoVermelho = 30,
              tempoAmarelo  = 5,
              tempoVerde    = 60;

// Pinos dos leds
byte  ledVermelhoTransito = 2,
      ledAmareloTransito  = 3,
      ledVerdeTransito    = 4,
      ledVermelhoPedestre = 5,
      ledVerdePedestre    = 6;fa

// Pino do botao
byte botao = 7;

// Pino do Buzzer
byte buzzer = 8;

/*========== Funçoes de controle ==========*/



void inicializaVariaveis(){
  pinMode(ledVermelhoTransito, OUTPUT);
  pinMode(ledAmareloTransito,  OUTPUT);
  pinMode(ledVerdeTransito,    OUTPUT);
  pinMode(ledVermelhoPedestre, OUTPUT);
  pinMode(ledVerdePedestre,    OUTPUT);

  pinMode(botao, INPUT_PULLUP);

  pinMode(buzzer, OUTPUT);
}

void ping(){
  tone(buzzer, 1000, 10);
  delay(10);
  no
}

void solicitaFechamento(){
  
  
}
 
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  
}

void loop() {
  // put your main code here, to run repeatedly:

}


