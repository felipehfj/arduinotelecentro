/*
 * Cabe�a seguidora de luz
 * Autor: Felipe Ferreira
 * 
 * Componentes necess�rios
 * Servo    : 1
 * LDR      : 2
 * Resistor : 2 x 10K
 */
#include <Servo.h>

#define ESPERA 500

Servo servo;
byte pinoServo = 6;
 
byte pinoSensorDireito  = A0;
byte pinoSensorEsquerdo = A1;

int valorSensorDireito  = 0;
int valorSensorEsquerdo = 0;

int valorMapeadoEsquerdo = 0;
int valorMapeadoDireito  = 0;

int posicao = 90;

void setup() {
  // put your setup code here, to run once:
  pinMode(pinoSensorEsquerdo, INPUT);
  pinMode(pinoSensorDireito,  INPUT);
  pinMode(pinoServo, OUTPUT);

  servo.attach(pinoServo);
}

void loop() {
  // put your main code here, to run repeatedly:
  calculaPosicao();
  servo.write(posicao);
  delay(ESPERA);
}

void calculaPosicao() {
  for (int i = 0; i < 10 ; i++) {
    valorSensorEsquerdo = valorSensorEsquerdo + analogRead(pinoSensorEsquerdo);
    valorSensorDireito  = valorSensorDireito  + analogRead(pinoSensorDireito);
  }

  valorSensorEsquerdo = constrain((valorSensorEsquerdo / 10), 0, 1023);
  valorSensorDireito  = constrain((valorSensorDireito  / 10), 0, 1023);

  valorMapeadoEsquerdo = map(valorSensorEsquerdo, 0, 1023, 0, 90);
  valorMapeadoDireito  = map(valorSensorDireito,  0, 1023, 0, 90);

  posicao = map((valorMapeadoEsquerdo - valorMapeadoDireito), -90, 90, 0, 180);
}