/*
 * Controle de n�vel de caixa d'agua
 * Vers�o: 0.1
 */

byte sensorPin = 3;
byte relePin = 4;
byte ledCaixaCheia = 5;
byte ledCaixaEnchendo = 6;

void setup() {
  // put your setup code here, to run once:
    pinMode(sensorPin, INPUT_PULLUP);
  pinMode(relePin, OUTPUT);
  pinMode(ledCaixaCheia, OUTPUT);
  pinMode(ledCaixaEnchendo, OUTPUT);

  digitalWrite(relePin, LOW);
  digitalWrite(ledCaixaCheia, LOW);
  digitalWrite(ledCaixaEnchendo, LOW);
}

void loop() {
  // put your main code here, to run repeatedly:
  if(digitalRead(sensorPin) == LOW){
    //Serial.println("Caixa est� cheia");
    digitalWrite(ledCaixaCheia, HIGH);
    digitalWrite(ledCaixaEnchendo, LOW);
  } else {
    encherCaixa();
  }
}

boolean encherCaixa(){
  digitalWrite(relePin, HIGH);
  digitalWrite(ledCaixaEnchendo, HIGH);  
  digitalWrite(ledCaixaCheia, LOW);
  
  for(;;){
    if(digitalRead(sensorPin) == LOW){
      digitalWrite(relePin, LOW);
      return true;
    }
  }
}
