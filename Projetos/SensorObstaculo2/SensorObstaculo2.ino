#include <PCM.h>
#include <NewPing.h>
#include "vozes.h"

int menu = 0;
NewPing ultrasonicFrente(9, 8);
NewPing ultrasonicDireita(7, 6);
NewPing ultrasonicEsquerda(5, 4);

unsigned int sdireita = 0;
unsigned int sesquerda = 0;
unsigned int sfrente = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:

  sfrente = ultrasonicFrente.ping_cm();
  sdireita = ultrasonicDireita.ping_cm();
  sesquerda = ultrasonicEsquerda.ping_cm();

  if (sfrente < 100) {
    
    if ((sdireita > 200) && (sesquerda > 200)) {
      Pare();
      Serial.println("A");
      VireAdireitaOUvireAesquerda();
    } else 
    if ((sdireita > 200) && (sesquerda < 100)) {
      Pare();
      Serial.println("B");
      VireAdireita();
    } else
    if ((sdireita < 100) && (sesquerda > 200)) {
      Pare();
      Serial.println("C");
      VireAesquerda();
    }else
    if ((sdireita < 100) && (sesquerda <100)) {
      Pare();
      Serial.println("D");
      Retorne();
    }
  
    while(sfrente < 100){      
      sfrente = ultrasonicFrente.ping_cm();
    }
  }

  Serial.print("Distancia a Frente(cm): ");
  // Pass INC as a parameter to get the distance in inches
  Serial.println(sfrente);

  Serial.print("Distancia a Direita(cm): ");
  // Pass INC as a parameter to get the distance in inches
  Serial.println(sdireita);

  Serial.print("Distancia a Esquerda(cm): ");
  // Pass INC as a parameter to get the distance in inches
  Serial.println(sesquerda);
  
  delay(100);

}

void Pare() {

  while (menu < 1) {
    menu++;

    Serial.println(menu);

    if (menu == 1) {
      startPlayback(pare, sizeof(pare));
    }

    for (int i = 0; i < 100; i++) {
      delay(10);
    }
  }
  menu = 0;

}

void Retorne() {

  while (menu < 1) {
    menu++;

    Serial.println(menu);

    if (menu == 1) {
      startPlayback(retorne, sizeof(retorne));
    }

    for (int i = 0; i < 100; i++) {
      delay(10);
    }
  }
  menu = 0;

}

void VireAdireita() {

  while (menu < 3) {
    menu++;

    Serial.println(menu);

    if (menu == 1) {
      startPlayback(vire, sizeof(vire));
    }

    if (menu == 2) {
      startPlayback(a, sizeof(a));
    }

    if (menu == 3) {
      startPlayback(direita, sizeof(direita));
    }

    for (int i = 0; i < 100; i++) {
      delay(10);
    }
  }
  menu = 0;

}

void VireAesquerda() {

  while (menu < 3) {
    menu++;

    Serial.println(menu);

    if (menu == 1) {
      startPlayback(vire, sizeof(vire));
    }

    if (menu == 2) {
      startPlayback(a, sizeof(a));
    }

    if (menu == 3) {
      startPlayback(esquerda, sizeof(esquerda));
    }

    for (int i = 0; i < 100; i++) {
      delay(10);
    }
  }
  menu = 0;

}

void VireAdireitaOUvireAesquerda() {

  while (menu < 7) {
    menu++;

    Serial.println(menu);

    if (menu == 1) {
      startPlayback(vire, sizeof(vire));
    }

    if (menu == 2) {
      startPlayback(a, sizeof(a));
    }

    if (menu == 3) {
      startPlayback(direita, sizeof(direita));
    }

    if (menu == 4) {
      startPlayback(ou, sizeof(ou));
    }

    if (menu == 5) {
      startPlayback(vire, sizeof(vire));
    }

    if (menu == 6) {
      startPlayback(a, sizeof(a));
    }

    if (menu == 7) {
      startPlayback(esquerda, sizeof(esquerda));
    }


    for (int i = 0; i < 100; i++) {
      delay(10);
    }
  }
  menu = 0;

}
