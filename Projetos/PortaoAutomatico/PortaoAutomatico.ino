/*
 * Portão Automático com Servo
 * Autor: Felipe Ferreira
 * Versão: 0.1
 * Data: 20JUL2017
 * 
 */
#include <Servo.h>

Servo servo;
byte pinServo = 6;
const int DELAY = 4 * 1000;

void setup() {
  // put your setup code here, to run once:
  servo.attach(pinServo);
}

void loop() {
  // put your main code here, to run repeatedly:
  abrirPortao(0, 90);
  delay(DELAY);
  fecharPortao(90, 0);
  delay(DELAY);
}

void abrirPortao(int inicio, int fim){
  for(int i = inicio; i <= fim; i++){
    servo.write(i);
    delay(10);
  }
}

void fecharPortao(int inicio, int fim){
  for(int i = inicio; i >= fim; i--){
    servo.write(i);
    delay(10);
  }
}

