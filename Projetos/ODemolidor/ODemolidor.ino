/*
 * App: O Demolidor
 * Aplicativo de auxilio a deficientes visuais em ambientes fechados
 * Versão: 1.0
 * Autores: Antonio Junior, Felipe Ferreira & Milton Soares
 */

typedef struct sensor {
  byte trigger;
  byte echo;
} Sensor;

const unsigned int DISTANCIALIMITE = 150;

unsigned int distanciaFrente   = 0;
unsigned int distanciaDireita  = 0;
unsigned int distanciaEsquerda = 0;

unsigned int distanciaFrenteMedia   = 0;
unsigned int distanciaDireitaMedia  = 0;
unsigned int distanciaEsquerdaMedia = 0;

Sensor sensorFrente   = {9, 8};
Sensor sensorDireita  = {7, 6};
Sensor sensorEsquerda = {5, 4};

void setup() {
  // put your setup code here, to run once:

  // Inicialização da comunicação serial
  Serial.begin(9600);

  // Definição do modo dos pinos dos sensores
  pinMode(sensorFrente.trigger, OUTPUT);
  pinMode(sensorFrente.echo, INPUT);
  pinMode(sensorDireita.trigger, OUTPUT);
  pinMode(sensorDireita.echo, INPUT);
  pinMode(sensorEsquerda.trigger, OUTPUT);
  pinMode(sensorEsquerda.echo, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  distanciaFrente   = 0;
  distanciaDireita  = 0;
  distanciaEsquerda = 0;
  distanciaFrenteMedia   = 0;
  distanciaDireitaMedia  = 0;
  distanciaEsquerdaMedia = 0;

  for (int i = 0; i < 10; i++) {
    // Obtenção dos dados de distancia do sensor da frente
    digitalWrite(sensorFrente.trigger, LOW);
    delayMicroseconds(2);
    digitalWrite(sensorFrente.trigger, HIGH);
    delayMicroseconds(10);
    digitalWrite(sensorFrente.trigger, LOW);
    distanciaFrente = pulseIn(sensorFrente.echo, HIGH);
    distanciaFrente = (distanciaFrente / 34 / 2); // Conversão para centímentros

    distanciaFrenteMedia += distanciaFrente;
    delay(25);
  }

  distanciaFrenteMedia /= 10;

  /*
     Verifica se a distancia obtida na frente é menor que
     a distância mínima necessária para efetuar a parada
     Caso afirmativo entra no teste
  */
  if (distanciaFrenteMedia <= DISTANCIALIMITE) {
    Serial.println("p");
    delay(1000);

    for (int i = 0; i < 10; i++) {
      // Obtenção dos dados de distancia do sensor da direita
      digitalWrite(sensorDireita.trigger, LOW);
      delayMicroseconds(2);
      digitalWrite(sensorDireita.trigger, HIGH);
      delayMicroseconds(10);
      digitalWrite(sensorDireita.trigger, LOW);
      distanciaDireita = pulseIn(sensorDireita.echo, HIGH);
      distanciaDireita = (distanciaDireita / 34 / 2); // Conversão para centímentros
      distanciaDireitaMedia += distanciaDireita;

      // Obtenção dos dados de distancia do sensor da direita
      digitalWrite(sensorEsquerda.trigger, LOW);
      delayMicroseconds(2);
      digitalWrite(sensorEsquerda.trigger, HIGH);
      delayMicroseconds(10);
      digitalWrite(sensorEsquerda.trigger, LOW);
      distanciaEsquerda = pulseIn(sensorEsquerda.echo, HIGH);
      distanciaEsquerda = (distanciaEsquerda / 34 / 2); // Conversão para centímentros
      distanciaEsquerdaMedia += distanciaEsquerda;
    }

    distanciaDireitaMedia  /= 10;
    distanciaEsquerdaMedia /= 10;

    if ((distanciaDireitaMedia > DISTANCIALIMITE) && (distanciaEsquerdaMedia > DISTANCIALIMITE)) { // Direita e esquerda é maior que a distância segura
      Serial.println("v"); // Vire a esquerda ou direita
    } else if ((distanciaDireitaMedia > DISTANCIALIMITE) && (distanciaEsquerdaMedia <= DISTANCIALIMITE)) { // Direita é maior que a distância segura e esquerda é menor que a distância segura
      Serial.println("d"); // Vire a direita
    } else if ((distanciaDireitaMedia <= DISTANCIALIMITE) && (distanciaEsquerdaMedia > DISTANCIALIMITE)) { // Direita é menor que a distância segura e esquerda é maior que a distância segura
      Serial.println("e"); // Vire a esquerda
    } else if ((distanciaDireitaMedia <= DISTANCIALIMITE) && (distanciaEsquerdaMedia <= DISTANCIALIMITE)) {// Direita e esquerda são menores que a distância segura
      Serial.println("r"); // Retorne
    }
    
    delay(2000);
  }

  //  do {
  //    // Obtenção dos dados de distancia do sensor da frente
  //    digitalWrite(sensorFrente.trigger, LOW);
  //    delayMicroseconds(2);
  //    digitalWrite(sensorFrente.trigger, HIGH);
  //    delayMicroseconds(10);
  //    digitalWrite(sensorFrente.trigger, LOW);
  //    distanciaFrente = pulseIn(sensorFrente.echo, HIGH);
  //    distanciaFrente = (distanciaFrente / 34 / 2); // Conversão para centímentros
  //  } while (distanciaFrente < DISTANCIAMINIMA);

  //imprimeDistancias();
  delay(50);
}

void imprimeDistancias() {
  Serial.print(F("E: "));
  Serial.print(distanciaEsquerdaMedia);
  Serial.print(F("\tF: "));
  Serial.print(distanciaFrenteMedia);
  Serial.print(F("\tD: "));
  Serial.println(distanciaDireitaMedia);
}
